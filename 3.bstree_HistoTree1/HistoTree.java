//(c) A+ Computer Science
//www.apluscompsci.com

//Name -

import static java.lang.System.*;

public class HistoTree
{
   private HistoNode root;

	public HistoTree( )
	{
		root = null;
	}

	public void addData(Comparable data)
	{
		add(data, root);
	}

	private void add(Comparable data, HistoNode tree)
	{
		if(root==null){
			root = new HistoNode(data);
			return;
		}
		int dir = data.compareTo(tree.getData());
		if(dir == 0)
			tree.addOne();
		else if(dir>0)
			if(tree.getRight()!=null)
				add(data, tree.getRight());
			else
				tree.setRight(new HistoNode(data));
		else if(dir<0)
			if(tree.getLeft()!=null)
				add(data, tree.getLeft());
			else
				tree.setLeft(new HistoNode(data));
	}

	public HistoNode search(Comparable data)
	{
		return search(data, root);
	}

	private HistoNode search(Comparable data, HistoNode tree)
	{
		if(tree == null) return null;
		int d = data.compareTo(tree.getData());
		if(d==0)
			return tree;
		else if (d>0) {
			return search(data, tree.getRight());
		}
		else if (d<0) {
			return search(data, tree.getLeft());
		}
		return null;
	}

	public String toString()
	{
		return toString(root);
	}

	private String toString(HistoNode tree)
	{
		if(tree!=null)
			return tree.toString();
		return null;
	}
}