//(c) A+ Computer Science
//www.apluscompsci.com

//Name -

public class ThingCount implements Comparable
{
	private int count;
	private Comparable thing;
	
	public ThingCount()
	{


	}
	
	public ThingCount(Comparable thang, int cnt)
	{
		thing = thang;
		count = cnt;
	}

	public void setThing(Comparable obj)
	{
		thing = obj;
		
	}
	
	public void setCount(int cnt)
	{
		count = cnt;

	}
	public void addOne()
	{
		count += 1;

	}
	
	public Comparable getThing()
	{
		return thing;
	}

	public int getCount()
	{
		return count;
	}
	
	public boolean equals(Comparable obj)
	{
		ThingCount other = (ThingCount)obj;



		return other.count==count&&other.thing.equals(thing);
	}
	
	public int compareTo(Object obj)
	{
		ThingCount other = (ThingCount)obj;
		return count>other.count?1:count==other.count?0:-1;		
	}
	
	public String toString()
	{
		return ""+thing + " - " + count;
	}
}