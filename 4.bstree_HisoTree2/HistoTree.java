//(c) A+ Computer Science
//www.apluscompsci.com

//Name -

public class HistoTree
{
   private TreeNode root;

	public HistoTree( )
	{

	}

	public void addData(Comparable data)
	{
		if(root==null){
			root = new TreeNode(new ThingCount(data , 1));
			return;
		}
		add(data, root);
	}

	private TreeNode add(Comparable data, TreeNode tree)
	{
		if(tree==null)
			return new TreeNode(new ThingCount(data, 1));
		int d = data.compareTo(((ThingCount)tree.getValue()).getThing());
		if(d==0)
			((ThingCount)tree.getValue()).addOne();
		else if(d<0)
			tree.setLeft(add(data, tree.getLeft()));
		else
			tree.setRight(add(data, tree.getRight()));
		return tree;
	}

	private TreeNode search(Comparable data)
	{
		return search(data, root);
	}

	private TreeNode search(Comparable data, TreeNode tree)
	{
		int d = data.compareTo(tree.getValue());
		if(d==0)
			return tree;
		else if (d>0) 
			return search(data, tree.getRight());
		return search(data, tree.getLeft());
	}

	public String toString()
	{
		return toString(root);
	}

	private String toString(TreeNode tree)
	{
		if(tree!=null)
			return toString(tree.getLeft())+" "+tree.getValue()+" "+toString(tree.getRight());
		return "";
	}
}