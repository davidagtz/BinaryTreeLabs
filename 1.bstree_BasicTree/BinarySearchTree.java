//(c) A+ Computer Science
//www.apluscompsci.com

//Name -

import static java.lang.System.*;
import java.util.LinkedList;

public class BinarySearchTree
{
	private TreeNode root;

	public BinarySearchTree()
	{
		root = null;
	}

	public void add(Comparable val)
	{
		root = add(val, root);
	}

	private TreeNode add(Comparable val, TreeNode tree)
	{
	   if(tree == null)
			tree = new TreeNode(val);

		Comparable treeValue = tree.getValue();
		int dirTest = val.compareTo(treeValue);
		if(dirTest < 0)
			tree.setLeft(add(val, tree.getLeft()));
		else if(dirTest > 0)
			tree.setRight(add(val, tree.getRight()));
		
		return tree;
	}

   public void inOrder()
	{
		inOrder(root);
		System.out.println("\n\n");
	}
	public void preOrder()
	{
		preOrder(root);
		System.out.println("\n\n");
	}
	public void postOrder()
	{
		postOrder(root);
		System.out.println("\n\n");
	}
	public void revOrder()
	{
		revOrder(root);
		System.out.println("\n\n");
	}

	private void inOrder(TreeNode tree)
	{
		if (tree != null){
			inOrder(tree.getLeft());
			System.out.print(tree.getValue() + " ");
			inOrder(tree.getRight());
		}
	}
	private String inOrderString(TreeNode tree)
	{
		String a = "";
		if (tree != null){
			a+=inOrderString(tree.getLeft());
			a+=tree.getValue() + " ";
			a+=inOrderString(tree.getRight());
		}
		return a;
	}

	//add preOrder, postOrder, and revOrder
	
	private void preOrder(TreeNode tree)
	{
		if (tree != null){
			System.out.print(tree.getValue() + " ");
			preOrder(tree.getLeft());
			preOrder(tree.getRight());
		}
	}
	private void postOrder(TreeNode tree)
	{
		if (tree != null){
			postOrder(tree.getLeft());
			postOrder(tree.getRight());
			System.out.print(tree.getValue() + " ");
		}
	}
	private void revOrder(TreeNode tree)
	{
		if (tree != null){
			revOrder(tree.getRight());
			System.out.print(tree.getValue() + " ");
			revOrder(tree.getLeft());
		}
	}
	public int getNumLevels()
	{
		return getNumLevels(root);
	}
	private int getNumLevels(TreeNode tree)
	{
		if(tree==null)
			return 0;
		else if(getNumLevels(tree.getLeft())>getNumLevels(tree.getRight()))
			return 1+getNumLevels(tree.getLeft());
		else
			return 1+getNumLevels(tree.getRight());
	}
	//add getNumLeaves, getWidth, getHeight, getNumNodes, and isFull

	public boolean isFull(){
		return isFull(root);
	}
	public boolean isFull(TreeNode a){
		if(a==null)
			return true;
		if((a.getRight()==null^a.getLeft()==null))
			return false;
		return true&&isFull(a.getRight())&&isFull(a.getLeft());
	}
	public int getNumLeaves(){
		return getNumLeaves(root);
	}
	public int getNumLeaves(TreeNode r){
		if(r==null)
			return 0;
		if(!r.hasChildren()){
			return 1;
		}
		return getNumLeaves(r.getRight())+getNumLeaves(r.getLeft());
	}
	public int getWidth(){
		return getWidth(root);
	}
	private int getWidth(TreeNode r){
		if(r==null)
			return 0;
		return 1+getNumLevels(r.getLeft())+getNumLevels(r.getRight());
	}
	public int getNumNodes(){
		return getNumNodes(root);
	}
	private int getNumNodes(TreeNode r){
		if(r==null)
			return 0;
		return 1+getNumNodes(r.getLeft())+getNumNodes(r.getRight());
	}






	//add extra credit options here - 10 points each
	
	//search
	public TreeNode search(Comparable a){
		return search(a, root);
	}
	public TreeNode search(Comparable a, TreeNode r){
		if(r==null)
			return null;
		int d = a.compareTo(r.getValue());
		if(d==0){
			return r;
		}else if(d>=1){
			return search(a, r.getRight());
		}else{
			return search(a, r.getLeft());
		}
	}
	
	//maxNode
	public TreeNode maxNode(){
		return maxNode(root);
	}
	public TreeNode maxNode(TreeNode r){
		if(r==null)
			return null;
		if(r.getRight()==null)
			return r;
		return maxNode(r.getRight());
	}
	//minNode
	public TreeNode minNode(){
		return minNode(root);
	}
	public TreeNode minNode(TreeNode r){
		if(r==null)
			return null;
		if(r.getLeft()==null)
			return r;
		return minNode(r.getLeft());
	}
	public Comparable minValue(TreeNode r){
		return minNode(r).getValue();
	}
	//level order
	
	//display like a tree
	
	//remove
	public void remove(Comparable a){
		remove(a, root);
	}
	public void remove(Comparable a, TreeNode r){
		if(r==null) return;
		int d = a.compareTo(r.getValue());
		if(d==0&&root==r){
			if(!r.hasNext()){
				root=null;
			}
			else if(r.hasChildren()){
				Comparable t =minValue(r.getRight()); 
				remove(t, root);
				r.setValue(t);
			}
			else if(r.getRight()!=null){
				root = r.getRight();
			}
			else if(r.getLeft()!=null){
				root = r.getLeft();
			}
			return;
		}
		else{
			int d2 = 0;
			d2 = r.get(d).getValue().compareTo(a);
			if(d2==0){
				if(!r.get(d).hasNext()){
					r.set(d, null);
				}
				else if(r.get(d).hasChildren()){
					Comparable temp = minValue(r.get(d));
					remove(temp, r);
					r.setValue(temp);
				}
				else if(r.get(d).getRight()!=null){
					r.setRight(r.get(d).getRight());
				}
				else if(r.getLeft().getLeft()!=null){
					r.setLeft(r.get(d).getLeft());
				}
			}
			else
				remove(a, r.get(d));
		}
	}


	public String toString()
	{
		return inOrderString(root);
	}

	private String toString(TreeNode tree)
	{
		return inOrderString(tree);
	}
}