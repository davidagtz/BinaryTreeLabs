//(c) A+ Computer Science
//www.apluscompsci.com

//Name -

import static java.lang.System.*;

public class BSTreeRunner
{
   public static void main( String args[] )
   {
 		//add test cases here
 		BinarySearchTree tree = new BinarySearchTree();
 		tree.add(90);
 		tree.add(80);
 		tree.add(70);
 		tree.add(85);
 		tree.add(100);
 		tree.add(98);
 		tree.add(120);
 		System.out.println("IN ORDER");
 		tree.inOrder();
 		System.out.println("PRE ORDER");
 		tree.preOrder();
 		System.out.println("POST ORDER");
 		tree.postOrder();
 		System.out.println("REVERSE ORDER");
 		tree.revOrder();
 		System.out.println("Tree height is "+(tree.getNumLevels()-1));
 		System.out.println("Tree width is "+tree.getWidth());
 		System.out.println("Number of leaves is "+tree.getNumLeaves());
 		System.out.println("Number of nodes is "+tree.getNumNodes());
 		System.out.println("Number of levels is "+tree.getNumLevels());
 		System.out.println("Tree string is "+tree.toString());
 		System.out.println("Tree is "+(tree.isFull()?"":"not ")+"full");
 		System.out.println("Tree does "+(tree.search(100)!=null?"":"not ")+"contains 100!");
 		System.out.println("Tree does "+(tree.search(114)!=null?"":"not ")+"contain 114!");
 		System.out.println("The smallest tree node "+tree.minNode().getValue());
 		System.out.println("The largest tree node "+tree.maxNode().getValue());
 		System.out.println("Tree before removing any nodes - using level order traversal.");
 		tree.preOrder();
		System.out.println("Tree after removing "+90);
		tree.remove(90);
 		tree.preOrder();
		System.out.println("Tree after removing "+70);
		tree.remove(70);
		System.out.print
 		tree.preOrder();ln("Tree after removing "+85);
		tree.remove(85);
 		tree.preOrder();
		System.out.println("Tree after removing "+98);
		tree.remove(98);
 		tree.preOrder();
		System.out.println("Tree after removing "+80);
		tree.remove(80);
 		tree.preOrder();
		System.out.println("Tree after removing "+120);
		tree.remove(120);
 		tree.preOrder();
		System.out.println("Tree after removing "+100);
		tree.remove(100);
 		tree.preOrder();

   }
}