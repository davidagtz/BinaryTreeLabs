//(c) A+ Computer Science
//www.apluscompsci.com

//Name -

public class TreeNode implements Treeable
{
	private Comparable treeNodeValue;
	private TreeNode leftTreeNode;
	private TreeNode rightTreeNode;

	public TreeNode( )
	{
		treeNodeValue = null;
		leftTreeNode = null;
		rightTreeNode = null;
	}

	public TreeNode(Comparable value)
	{
		treeNodeValue = value;
		leftTreeNode = null;
		rightTreeNode = null;
	}

	public TreeNode(Comparable value, TreeNode left, TreeNode right)
	{
		treeNodeValue = value;
		leftTreeNode = left;
		rightTreeNode = right;
	}

	public Comparable getValue()
	{
		return treeNodeValue;
	}

	public TreeNode getLeft()
	{
		return leftTreeNode;
	}

	public TreeNode getRight()
	{
		return rightTreeNode;
	}
	public TreeNode get(int dir){
		if(dir <= -1)
			return getLeft();
		else if(dir >= 1)
			return getRight();
		return null;
	}

	public void setValue(Comparable value)
	{
		treeNodeValue = value;
	}

	public void setLeft(Treeable left)
	{
		leftTreeNode = (TreeNode)left;
	}

	public void set(int dir ,Treeable left)
	{
		if(dir <= -1)
			leftTreeNode = (TreeNode)left;
		else if(dir >= 1)
			rightTreeNode = (TreeNode)left;	
	}

	public void setRight(Treeable right)
	{
		rightTreeNode = (TreeNode)right;
	}
	public boolean hasChildren(){
		return getRight()!=null&&getLeft()!=null;
	}
	public boolean hasChild(){
		return getRight()!=null^getLeft()!=null;
	}
	public boolean hasNext(){
		return getRight()!=null||getLeft()!=null;
	}
}